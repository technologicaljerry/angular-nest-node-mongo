import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(process.env.SERVER_PORT);

  app.enableCors({
    origin: ['http://localhost:4700'],
    // origin: true,
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    credentials: true,
  });
  Logger.log(`🚀 🚀 🚀 🚀 🚀 Server running on HOST => ${process.env.HOST_NAME}`, 'Bootstrap');
  Logger.log(`🚀 🚀 🚀 🚀 🚀 Server running on PORT => ${process.env.SERVER_PORT}`, 'Bootstrap');
}
bootstrap();
